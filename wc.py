class WC:
    f = None
    
    def __init__(self, namafile):
        self.txt = namafile
       

    def word_count(self):
        self.f = open(self.txt,'rt') 
        kata = 0  
        for line in self.f : 
            kata += len(line.split(" "))
        self.f.close()
        return kata
        

    def line_count(self):
        self.f = open(self.txt,'rt') 
        lines = 0  
        for line in self.f : 
            lines += 1
        self.f.close()
        return lines

    def char_count(self):
        self.f = open(self.txt,'rt') 
        char = 0  
        for line in self.f : 
            char += len(line)
        self.f.close()
        return char