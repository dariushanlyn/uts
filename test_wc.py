import unittest
from wc import WC

class TestWC(unittest.TestCase):
    def test_wc(self):
        n = WC('README.md')
        self.assertEqual(0, n.word_count())
        self.assertEqual(0, n.line_count())
        self.assertEqual(0, n.char_count())
